var gl;
var points;
var numPoints = 500000;

window.onload = function init() {
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL is not available!")
    }
//three vertices

    var vertices = [
        vec2(-1, -1),
        vec2(0, 1),
        vec2(1, -1)
    ];


    var u = scale(0.5, add(vertices[0], vertices[1]));
    var v = scale(0.5, add(vertices[0], vertices[2]));
    var p = scale(0.5, add(u,v));
// setting an initial point
    points = [p];

    //generate all the points
    for(var i=1; i < numPoints; i++){
        //get a random corner of the triangle
        var j = Math.floor(Math.random()*3);
        //get the point halfway between the corner and the previous point
        p = scale(0.5, add(points[i-1], vertices[j]))
        points.push(p)
    }


// Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
// Load shaders and init attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);
    // Load the data into GPU
    var bufferID = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferID);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW);
//Associate out shader variables with our data buffer
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);
    render();
}
function render() {
    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.POINTS, 0, numPoints);
}